\chapter{DOA Ultrasound Outdoor Experiment}\label{chapter:experiment}

\section{Ultrasound Frequency Range of Interest}

Sound is usually called ultrasound when it primarily consists of frequencies above 20 kHz and thus is inaudible for human beings. In the scope of this thesis we want to perform acoustic localization of ultrasonic signals via the DOA method. In order to diminish the frequency range we only consider signals in the interval $[35, 40]$ kHz. Hence, we are not dealing with normal baseband signals but rather with band-limited signals with a total bandwidth of 5 kHz.
This choice is primarily motivated by the fact that this allows us to perform Sub-Nyquist sampling with a lower sampling rate and still avoiding alias effects in the temporal domain. More specifically, we are using a sampling rate of 11.5 kHz which is exactly 2.3 times the bandwidth of our band-limited signals instead of using the normal Nyquist frequency of $f_s > 2 \cdot 40$ kHz.

\section{Expectations from Simulation}\label{sec:simulation}

\textit{Pyroomacoustics}\footnote{\url{https://github.com/LCAV/pyroomacoustics}} \autocite{pyroomacoustics} is a python package for audio room simulations and array processing simulations. This includes reference implementations of DOA algorithms which are used in this thesis. Since it also provides room impulse simulations realized via the image source model \autocite{imagesourcemodel}, it can used for the simulation of rooms with specific parameters. Hence, a complete DOA performance simulation can be carried out in a synthetic environment.
Thus, I performed a DOA evaluation of the MUSIC, CSSM, WAVES, and FRIDA algorithm with pyroomacoustics with the intention to simulate the results of the physical microphone array in the real world. The following parameters were used:
\begin{itemize}
    \item Anechoic room with dimensions of 30 $\times$ 20 meters
    \item SNR of 20 dB
    \item Speed of sound $c$ of 343 m/s (at 20 degrees Celsius)
    \item Detailed mapping of the physical array geometry by using an inter-microphone distance of 0.066 meters
    \item Placement of the microphone array at the center of the room
    \item Two simultaneous active, separate sources using subsampled (sampling rate: 11.5 kHz) ultrasonic signals in the 35-40 kHz range
    \item The sources were placed at random azimuth locations from the full 0-360 degree interval with a distance of 5 meters from the microphone array
    \item For the STFT a FFT size of 256 points, matching the block size of audio samples, with 50\% overlap and no windowing function were used
\end{itemize}
For the evaluation of the performance the reconstructed azimuth angles from the algorithms were compared with the real reference angles, yielding the absolute error\footnote{Code of both experiments available at: \url{https://gitlab.com/L0g4n/ultrasound-localization-software}}.

\begin{figure}[ht]%
    \centering
    \subfloat[CSSM]{{\includegraphics[width=7cm]{synth_exp/CSSM.pdf}}}%
    \quad
    \subfloat[WAVES]{{\includegraphics[width=7cm]{synth_exp/WAVES.pdf}}}\\%
    \subfloat[MUSIC]{{\includegraphics[width=7cm]{synth_exp/MUSIC.pdf}}}%
    \quad
    \subfloat[FRIDA]{{\includegraphics[width=7cm]{synth_exp/FRIDA.pdf}}}%
    \caption{Singular results from simulation. The green lines indicate the spatial spectrum from which the maxima are picked as azimuth candidates. It can be seen that MUSIC performs the best overall.}%
    \label{fig:simulation_singular_results}%
\end{figure}

The results from the simulation indicate that the MUSIC algorithm is the most promising one since it consistently delivers good results with only a few outliers; It usually is able to localize sources up to 10 degrees in the full 360 degrees interval if they are not closer spaced than 20 degrees. However, MUSIC seems to break down eventually when trying to localize three simultaneous active sources. This indicates that the size of the microphone array, four individual microphones, seems to be the limiting factor here.
FRIDA's results on the other hand seem to very volatile, CSSM and WAVES seem to have problems localizing more than one source since the reconstructed azimuth values for two sources are usually wrong by a large margin all the time, see \autoref{fig:simulation_singular_results}. Considering the observations from the simulation, I therefore expect MUSIC to yield consistently accurate results, followed by the volatile results from FRIDA and the poor results from CSSM and WAVES.
However, what I do not now know up to this point if the subsampling will work as unproblematic as experienced in the simulation.

Note that this section did not have the goal to present statistically validated results as it only presents individual cases and not a larger number of trials, but rather fuel feasible expectations of the results I will raise in the next section where the outcomes from the outdoor ultrasound experiments are presented.

\section{Outdoor Experiment}\label{sec:outdoor_experiment}

The physical microphone array was placed in a backyard and its job was to localize an ultrasonic signal emitted from a ultrasound transmitter operating at an approximate frequency of 39.9 kHz. The origin of the microphone array is located at a height of approximately 47 cm with an inter-microphone spacing of 5.6 cm which was the smallest physically feasible distance since the microphones alone have a diameter of roughly 2.5 cm. The transmitter was placed on top of a stick at a height of 52 cm. For some images of the experiment, see \autoref{fig:outdoor_exp_setup}.

\begin{figure}[ht]%
    \centering
    \subfloat[The front of the microphone array.]{{\includegraphics[width=7cm]{array_front}}}%
    \quad
    \subfloat[The ultrasound transmitter operating at a frequency of 39.9 kHz.]{{\includegraphics[width=7cm]{exp/us_sender}}}%
    \caption{The setup of the outdoor experiment.}%
    \label{fig:outdoor_exp_setup}%
\end{figure}

The ultrasonic source was placed at two azimuth locations, 0 and 45 degrees, with a distance to the array of 2 meters. For each azimuth location all algorithms have been executed 50 times and the absolute integer error of the DOA localization (the difference of the reconstruction and the ground-truth) has been stored. The microphones were only sampled once in the beginning such that all algorithms operate on the same data. On this particular day the outdoor temperature was 7 to 8 degrees Celsius which was used to calculate the speed of sound; occasional gusts with a wind speed of roughly 32 km/h were also present. Apart from that, the parameters from \autoref{sec:simulation} were adopted.

The results\footnote{Trial data and evaluation code available at: \url{https://gitlab.com/L0g4n/ultrasound-localization-software/-/tree/master/trial_data}. Note that the presented results are in the \textit{exp2} directory.} can be found in \autoref{fig:exp_results}.
It can be the seen that the overall accuracy of all DOA algorithms is bad since the smallest mean absolute error (MAE) is 64 degrees for FRIDA, the highest is 118 degrees for SRP. Furthermore, the smallest occurring deviation from the ground truth also delivers FRIDA with about 12 degrees. However, in total, all algorithms deliver strong fluctuating results that are far off the actual azimuth location and thus renders proper localization via DOA method in this case impossible.
Naturally, one might ask what is the reason for such bad results for localizing only one source in the real world since the simulation showed that we should be able to accurately locate at least two simultaneously active sources with microphone array of size four?

\begin{figure}[ht]%
    \centering
    \subfloat[CSSM]{{\includegraphics[width=7cm]{exp/cssm_exp2_mae.pdf}}}%
    \quad
    \subfloat[WAVES]{{\includegraphics[width=7cm]{exp/waves_exp2_mae.pdf}}}\\%
    \subfloat[SRP]{{\includegraphics[width=7cm]{exp/srp_exp2_mae.pdf}}}%
    \quad
    \subfloat[FRIDA]{{\includegraphics[width=7cm]{exp/frida_exp2_mae.pdf}}}\\%
    \subfloat[MUSIC]{{\includegraphics[width=7cm]{exp/music_exp2_mae.pdf}}}%
    \caption{The compiled results of the absolute error the specified algorithm with the resulting mean absolute error (MAE).}%
    \label{fig:exp_results}%
\end{figure}

The primary reason for this phenomenon is \textit{spatial aliasing} which are effectively aliasing effects not in the temporal but rather in the spatial domain. One limiting factor of DOA localization in general is that for proper localization of a signal with frequency $f$ and thus its wavelength $\lambda$ the microphones in the array should be spaced with a distance of half or quarter of the wavelength $\lambda$, not bigger than the wavelength. Otherwise, spatial aliasing occurs which has the unpleasant impact of introducing ambiguity in the localization process, see for reference \autocite{spatialaliasing}. Consequently, when the spatial spectrum is plotted peaks with similar magnitudes at several locations can be observed, see \autoref{fig:exp_spatial_aliasing}; From these various extrema the angles for the azimuth reconstruction are picked. This is aspect is justified by the fact that these DOA algorithms rely on the phenomenon that sources at different locations have different phase difference between the microphones deployed in the array. However, when the distance between the microphones is too big (larger than half the wavelength for example), the phase difference is no longer unique which results in not being able to resolve the angle. The aforementioned aspects play the key role in this experiment: The microphones deployed in the array do not fulfill the physical constraints as they alone have a diameter of 2.5 cm and such the minimal physically feasible inter-microphone distance was 5.6 cm (which was the inter-microphone distance used in the outdoor experiment). This is too big since the wavelength of a 40 kHz is at 20 degrees Celsius roughly 0.86 cm ($\frac{343}{40e3} = 0.008575 m$) and 5.6cm is thirteen times bigger than half the wavelength of 0.43 cm.

Naturally, the follow-up question is: What can be done to bypass this problem?
The obvious and nearby answer is to use microphones that are much smaller in size, e.g. Microelectromechanical systems (MEMS) microphones which only have a size of a few millimeters and hence could be deployed in this very application context.
Another approach could be to investigate if a certain pre-processing technique could be used to exclude those false positives caused by the spatial aliasing by matching the output of different microphone pairs. Due to time constraints this could not be further investigated.
In addition, the aforementioned issue could be solved by deploying a different microphone array. Instead of using a stationary microphone, a distributed microphone array could be used (which would then need to communicate messages amongst each other). The microphones in the distributed array would not have these physical constraints. However, the obligatory communicational aspect would require some sort of network connectivity which increases overall system latency.

\begin{figure}[ht]%
    \centering
    \subfloat[CSSM]{{\includegraphics[width=7cm]{exp/cssm_spatial}}}%
    \quad
    \subfloat[WAVES]{{\includegraphics[width=7cm]{exp/waves_spatial}}}\\%
    \subfloat[SRP]{{\includegraphics[width=7cm]{exp/srp_spatial}}}%
    \quad
    \subfloat[FRIDA]{{\includegraphics[width=7cm]{exp/frida_spatial}}}\\%
    \subfloat[MUSIC]{{\includegraphics[width=7cm]{exp/music_spatial}}}%
    \caption{Illustrations of the present spatial aliasing effects that disrupt the performance of the DOA algorithms. Several peaks of similar magnitude denote aliasing from which the DOA algorithm picks to yield the angle.}%
    \label{fig:exp_spatial_aliasing}%
\end{figure}
