\section{MUSIC}\label{section:music}
\textit{MUSIC}, Multiple Signal Classification, introduced by Schmidt in \autocite{music}, is a DOA algorithm that relies on subspace decomposition of the signal and noise space, i.e. the signal subspace -- received from the measured data from the array -- is explicitly separated from the noise subspace. Basically, the eigenvectors of the covariance matrix -- formed by the signals received in the microphones -- span the signal subspace, the noise space eigenvectors span the span the noise subspace.
This is supported by the following data model
\begin{equation}\label{equation:music_data_model}
    \nonumber\begin{bmatrix}
        X_1\\
        X_2\\
        \vdots\\
        X_M
    \end{bmatrix} =
    \begin{bmatrix}
        \boldsymbol{a}(\theta_1) & \boldsymbol{a}(\theta_2) & \cdots & \boldsymbol{a}(\theta_D)
    \end{bmatrix}
    \begin{bmatrix}
        F_1\\
        F_2\\
        \vdots\\
        F_D
    \end{bmatrix}
    +
    \begin{bmatrix}
        W_1\\
        W_2\\
        \vdots\\
        W_M
    \end{bmatrix} \iff \boldsymbol{X} = \boldsymbol{AF} + \boldsymbol{W}
\end{equation}
where $\boldsymbol{X}$ are the signals received at the $M$ microphone array which are represented as linear combinations of the $D$ source wavefronts and some additive noise, $\boldsymbol{W}$.
The source signals comprise the amplitude and phase information at some reference point by the complex quantities $\boldsymbol{F}$.
The row vector $\boldsymbol{A}$ contains directional mode vectors $\boldsymbol{a}(\theta_j) = a_{ij}$ for $i = 1, 2, \dots, M$ which are functions of the signal DOA angles and the locations of the array.
More specifically, $a_{ij}$ depends on the $i$-th microphone, its position relative to the origin of the local coordinate system, and its response to the source signal from the direction of the $j$-th signal.
The mode vector $a(\theta_j)$ is the response to the DOA $\theta_j$ of the $j$-th signal. This implies that knowing $\boldsymbol{a}(\theta_j)$ reveals $\theta_j$.

Futhermore, $\boldsymbol{X}$ is constrained to the top by dimensions of the range space of $\boldsymbol{A}$. The whole continuum of DOA vectors is denoted by simply $\boldsymbol{a}(\theta)$.
Now, the problem of solving for the DOA of multiple sources is formulated by \enquote{[\ldots] locating the intersections of the $\boldsymbol{a}(\theta)$ continuum within the range space of $\boldsymbol{A}$} \autocite{music}; The range space is obtained from the measurements of the microphone signals.

\subsection{The Covariance Matrix}
The covariance matrix, $\boldsymbol{S}$, of the $\boldsymbol{X}$ vector is a $M \times M$ matrix resulting from
\begin{equation}\label{equation:music_covariance_matrix}
    \boldsymbol{S} = \overline{\boldsymbol{XX}^*} = \boldsymbol{A}\overline{\boldsymbol{FF}^*}\boldsymbol{A}^* + \overline{\boldsymbol{WW}^*} \iff \boldsymbol{S} = \boldsymbol{APA}^* + \lambda \boldsymbol{S}_0
\end{equation}
under the assumption that the source signals and the noise are uncorrelated, $\lambda$ denotes the eigenvalue, $\boldsymbol{S}_0$ the noise covariance matrix.
When $\boldsymbol{A}$ is full rank and $\boldsymbol{P}$ positive definite, $\boldsymbol{APA}^*$ must be nonnegative definite; This means that $\lambda$ has to be the minimum eigenvalue $\lambda_{min}$ and the measured covariance matrix $\boldsymbol{S}$ can be rewritten as
\begin{equation}\label{equation:music_covariance_rewritten}
    \boldsymbol{S} = \boldsymbol{APA}^* + \lambda_{min}\boldsymbol{S}_0,\quad \lambda_{min} \geq 0
\end{equation}
with $\lambda_{min}$ being the smallest solution to $\lvert \boldsymbol{S} - \lambda \boldsymbol{S}_0 \rvert = 0$.
Futhermore, the rank of $\boldsymbol{APA}^*$ is $D$ and can be determined from the eigenvalues of $\boldsymbol{S}$ in the metric of $\boldsymbol{S}_0$ \autocite{music}.

\autocite{music} also proposes a way to estimate the number of sources present in the observed data. It is argued that complete set of eigenvalues of $\boldsymbol{S}$ in the metric of $\boldsymbol{S}_0$ will not always be simple; In fact, this will occur repeatedly
$N = M - D$ times because the eigenvalues of $\boldsymbol{S}$ and $\boldsymbol{S} - \lambda_{min} \boldsymbol{S}_0 = \boldsymbol{APA}^*$ will differ always by exactly $\lambda_{min}$. Futhermore, the minimum eigenvalue of $\boldsymbol{APA}^*$ is zero in the singular matrix case which means that
$\lambda_{min}$ occurs specifically $N$ times. Knowing that, a number of sources signal estimator can be formulated by
\begin{equation}\label{equation:music_signals_estimator}
    \hat{D} = M - \hat{N}
\end{equation}
where $\hat{N}$ is the multiplicity of $\lambda_{min}(\boldsymbol{S}, \boldsymbol{S}_0)$ which reads as \enquote{$\lambda_{min}$ of $\boldsymbol{S}$ in the metric of $\boldsymbol{S}_0$} \autocite{music}.

\subsection{The Description of the Algorithm}
We now want to solve for the source signal mode vectors. $\boldsymbol{E}_N$ is a $M \times N$ matrix whose columns are the $N$ noise eigenvectors, and $d^2$ is the normal (squared) Euclidean distance from vector $\boldsymbol{Y}$ to the signal subspace with
$d^2 = \boldsymbol{Y}^* \boldsymbol{E}_N \boldsymbol{E}_N^* \boldsymbol{Y}$. $P_{MU}(\theta)$ is the function that plots exactly $1/d^2$ points along the $\boldsymbol{a}(\theta)$ continuum with
\begin{equation}\label{equation:music_PMU}
    P_{MU}(\theta) = \frac{1}{\boldsymbol{a}^*(\theta)\boldsymbol{E}_N \boldsymbol{E}_N^* \boldsymbol{a}(\theta)}
\end{equation}
After the DOA of the $D$ source signals have been computed, $\boldsymbol{A}$ becomes available to compute other parameters of the source signals.
Since $\boldsymbol{APA}^* = \boldsymbol{S} - \lambda_{min}\boldsymbol{S}_0$, we get
\begin{equation}\label{equation:music_other_parameters}
    \boldsymbol{P} = (\boldsymbol{A}^* \boldsymbol{S}_0^{-1}\boldsymbol{A})^{-1} \boldsymbol{A}^* \boldsymbol{S}_0^{-1}(\boldsymbol{S}-\lambda_{min}\boldsymbol{S}_0)\boldsymbol{S}_0^{-1}\boldsymbol{A}(\boldsymbol{A}^* \boldsymbol{S}_0^{-1}\boldsymbol{A})^{-1}
\end{equation}
if the noise covariance matrix, $\boldsymbol{S}_0$, is \textbf{not} the identity matrix and requires whitening \autocite{music}.
A summary of whole procedure of the algorithm is depicted in \autoref{pseudo:music}.
In step 3 it can be seen that the number of source signals has to be set as a parameter for the algorithm. This can either be set to a fixed value known a priori or can be estimated from the observed data (e.g. \autoref{equation:music_signals_estimator}).
\pseudo{MUSIC}{pseudo:music}{music}
