\section{Comparison of DOA Algorithms}\label{section:comparison_doa}

In this section a brief comparison of merits and demerits of the aforementioned DOA algorithms is provided.
In \autoref{table:doa_classification} the theoretical classification for all algorithms is summarized.

\begin{table}[ht]
\centering
\begin{tabular}{l c}
    \toprule
    \textbf{DOA Algorithm} & \textbf{Classification Type} \\
    \midrule
    SRP-PHAT & Filter-and-sum beamforming \\
    MUSIC & Subspace-based method, incoherent \\
    CSSM & Subspace-based method, coherent \\
    WAVES & Subspace-based method, coherent \\
    FRIDA & FRI sampling-based method \\
    \bottomrule
\end{tabular}
\caption{Theoretical classification of the selected DOA algorithms.}
\label{table:doa_classification}
\end{table}

As MUSIC, CSSM, and WAVES all exploit the orthogonality between the signal and noise subspace, they are located in the field of \textit{subspace algorithms}; SRP-PHAT combines the steered response power beamformer with the PHAT weighting function and thereby is a \textit{beamforming} technique. Last, but not least, FRIDA is somehow more decoupled from the rest since it employs a novel \textit{FRI sampling algorithm}.

Common features of  DOA algorithms in general include for example: The granularity of the space resolution, the SNR range in which it still can function effectively, the constraints raised on the microphone array geometry, the efficiency of the algorithm, the number of sources that can be resolved, the degree to which closely spaced sources can be resolved.
A non-exhaustive list of a few merits and drawbacks of the selected DOA algorithms follows.

\textbf{SRP-PHAT}:
\begin{itemize}
    \item[$+$] Can handle coherent signals, i.e. suitable for the multi-path case in mildly reverberant conditions \autocite[chap. 7.1]{srpphat}.
    \item[$+$] Performs also good in high noise (low SNR) conditions \autocite[chap. 7.3.2]{srpphat}.
    \item[$+$] Can resolve more sources than microphones in the array.
    \item[$-$] Through beamforming inherently no high resolution, can not resolve closely spaced sources.
\end{itemize}

\textbf{MUSIC}:
\begin{itemize}
    \item[$+$] High resolution subspace method.
    \item[$-$] Can not resolve more sources than microphones through the limitations of the subspace method.
    \item[$-$] In its baseform limited to narrowband signals.
\end{itemize}

\textbf{CSSM}:
\begin{itemize}
    \item[$+$] High resolution subspace method.
    \item[$+$] Subspace method for wideband signals.
    \item[$-$] Very sensitive to model errors, such as wrong estimates of the number of sources \autocite{waves}.
    \item[$-$] Can not resolve more sources than microphones through the limitations of the subspace method.
    \item[$-$] Since its a coherent method, it requires focusing matrices, initial guesses of the source locations, i.e., sensitive preprocessing.
    \item[$-$] Generally, coherent methods are less robust to noise, i.e. more sensitive to lower SNRs.
\end{itemize}

\textbf{WAVES}:
\begin{itemize}
    \item[$+$] High resolution subspace method.
    \item[$+$] Subspace method for wideband signals.
    \item[$+$] Less sensitive to focusing errors than CSSM \autocite{waves}.
    \item[$-$] Can not resolve more sources than microphones through the limitations of the subspace method.
    \item[$-$] Since its a coherent method it requires focusing matrices, initial guesses of the source locations, i.e., sensitive preprocessing.
    \item[$-$] Generally, coherent methods are less robust to noise, i.e. more sensitive to lower SNRs.
\end{itemize}

\textbf{FRIDA}:
\begin{itemize}
    \item[$+$] Suitable for wideband signals.
    \item[$+$] Requires as the only one no grid search over the solution space.
    \item[$+$] Less complexity than the rest of the subspace methods.
    \item[$+$] Promises very high resolution at low SNRs.
    \item[$+$] Can resolve more sources than microphones in the array.
    \item[$-$] Only works in 2D by default, i.e. azimuth resolution.
    \item[$-$] Being able to handle more sources than microphones means it can not handle completely correlated signals \autocite{frida}, making it less suitable for reverberant conditions.
\end{itemize}

All the subspace based methods like MUSIC, CSSM, and WAVES share for example the limitation that they can not resolve more sources than microphones deployed in the array. This is because they compute the $M \times M$ spatial covariance matrix, $M$ being the amount of microphones, and then perform an eigenvalue decomposition to extract the signal and noise subspaces. More specifically, only $M - 1$ sources can be resolved since at least one dimension is reserved for the noise.
However, the last mentioned limitation is not relevant for me at all since the situation of wanting to resolve more sources that microphones does not apply for me.
Additionally, the suitability for wideband signals is not needed since I am only observing ultrasonic signals with a bandwidth of 5 kHz (up to a frequency of 40 kHz).
