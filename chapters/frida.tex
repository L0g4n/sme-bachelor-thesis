\section{FRIDA}\label{section:frida}

\textit{FRIDA}, finite rate of innovation (FRI) for DOA finding, is yet another algorithm for estimating the DOAs of multiple wide-band sources. Introduced in \autocite{frida}, it aims to provide high spatial resolution at low signal-to-noise ratios (SNR), work with arbitrary microphone array geometries and \textbf{does not} require a \textit{grid search} over space like all the DOA algorithms introduced before.
FRIDA is classified as a sampling-based method since it works at its core with a finite rate of innovation sampling algorithm. The gist of the algorithm is that for any array geometry the elements of the spatial correlation matrix can linearly transformed into uniformly sampled sums of sinusoids \autocite{frida}.

\subsection{Signal Model}

A setup of $Q$ microphones located at $\{ \mathbf{r_q} \in \mathbb{R}^2 \}_{q=1}^Q$ with $K$ monochromatic, uncorrelated point sources in the far-field, indexed by letter $k$, is assumed.
Each source propagates in the direction of the unit vector $\mathbf{p}_k \in \mathbb{S}$ where $\mathbb{S}$ is the unit circle. Considering the narrow band centered at a frequency $\omega$, the baseband representation of the signal from direction $\mathbf{p} \in \mathbb{S}$ is denoted by $x(\mathbf{p}, \omega, t) = \tilde{x}(\mathbf{p}, \omega)e^{j\omega t}$ where $\tilde{x}(\mathbf{p}, \omega)$ is the emitted sound signal by the specified source at location $\mathbf{p}$ and frequency $\omega$.
The \textit{intensity} of the sound field is defined by
\begin{equation}\label{equation:frida_intensity}
I(\mathbf{p}, \omega) = \mathbb{E}[ \lvert x(\mathbf{p}, \omega, t) \rvert^2] = \mathbb{E}[ \lvert \tilde{x}(\mathbf{p}, \omega) \rvert^2]
\end{equation}
The received signal at the $j$-th microphone at position $\mathbf{r}_q$ consists of all plane waves along the unit circle
\begin{equation}\label{equation:frida_received_signal}
y_q(\omega, t) = \int_{\mathbb{S}} x(\mathbf{p}, \omega, t)e^{-j\omega \langle \mathbf{p}, \frac{\mathbf{r}_q}{c} \rangle} d\mathbf{p} \quad for q=1, \dots, Q
\end{equation}
where c is the speed of sound \autocite{frida}. As measurements of the signals -- and thus as input to the algorithm -- not the raw signals are used but the \textit{cross-correlations} between the received signals for a microphone pair $(q, q')$:
\begin{equation}\label{equation:frida_cc}
V_{q,q'}(\omega) = \mathbb{E}[ y_q(\omega, t) y_{q'}^*(\omega, t)]
\end{equation}
where $q, q' \in [1, Q]$ and $q \neq q'$ \autocite{frida}.
Since point sources are assumed, the source distribution is seen as a sum of spatially localized sources and thus
\begin{equation}
\tilde{x}(\mathbf{p}, \omega) = \sum_{k=1}^K \alpha_k(\omega) \sqrt{\gamma} \phi (\gamma(\mathbf{p} - \mathbf{p}_k))
\end{equation}
where $\phi(\mathbf{p})$ is a localized function of the source with finite energy,$\int \phi^2 d\mathbf{p} = 1$, $\gamma$ is a spatial scaling factor \autocite{frida}.

\subsection{The FRIDA Algorithm}

The generalized FRI sampling framework is depicted in \autoref{fig:fri_framework}. FRIDA follows this framework in the structure of its algorithm and contains two major steps:
\begin{enumerate}
    \item Identify the set of unknown sinusoidal samples and the relationship with the cross-correlation measurements (\autoref{equation:frida_cc})
    \item Do DOA estimation as a constrained optimization problem
\end{enumerate}

\image{\textwidth}{fri_framework}{
The generalized FRI sampling framework. Generally, as soon as a continuous domain signal can be transformed into a finite sum of sinusoids, it is a \textit{FRI} signal.
The FRI sampling problem then consists of estimating the frequencies of the sinusoids from the given measurements \autocite{fri}.
}{fig:fri_framework}

\subsubsection{Relationship between measurements and sinusoids}

The intensity of the sound field can be represented as a Fourier series with
\begin{equation}
    I(\mathbf{p}, \omega) = \sum_{m \in \mathbb{Z}} \hat{I}_m(\omega)Y_m(\mathbf{p})
\end{equation}
where $Y_m(\mathbf{p})$ is the Fourier series basis $Y_m(\mathbf{p}) = Y_m(\phi) = e^{jm\phi}$, and $\hat{I}_m(\omega)$ is the expansion coefficient for a sub-band centered at frequency $\omega$ \autocite{frida} with
\begin{equation}\label{equation:frida_ihat}%
\hat{I}_m(\omega) = \frac{1}{2\pi} \int_{\mathbb{S}} I(\mathbf{p}, \omega) Y_m^*(\mathbf{p}) d\mathbf{p} = \frac{1}{2\pi} \sum_{k=1}^K \sigma_k^2(\omega)e^{-jm\phi k}%
\end{equation}%
A \textit{linear mapping} is established from the uniformly sampled sinusoids $\hat{I}_m$ to the measurements $V_{q,q'}$.
Thereby, a lexicographically ordered vectorization $\mathbf{a}(\omega) \in \mathbb{C}^{Q(Q-1)}$ of the cross-correlations $V_{q, q'}(\omega)$ is introduced, and the vector $\mathbf{b}(\omega)$ consisting of the Fourier series coefficients $\hat{I}_m(\omega)$ for $m \in \mathcal{M}$, where $\mathcal{M}$ is the set of considered Fourier coefficients.
Also, the $Q(Q-1) \times \lvert \mathcal{M} \rvert$ matrix $\mathbf{G}(\omega)$ is introduced which rows consist of microphone pairs $(q, q')$ and its columns of Fourier bins $m$:
\begin{equation}
    g_{(q, q'), m}(\omega) = (-j)^m J_m (\lVert \omega \Delta \mathbf{r}_{q,q'} \rVert_2) Y_m \left(  \frac{\Delta \mathbf{r}_{q,q'}}{\lVert \Delta \mathbf{r}_{q,q'} \rVert_2} \right)
\end{equation}
Thus, the relationship between measurements and sinusoids \autocite{frida} can be shortly written as:
\begin{equation}%
\mathbf{a}(\omega) = \mathbf{G}(\omega) \mathbf{b}(\omega)%
\end{equation}%

\subsubsection{Annihilation}
The weighted sum of sampled sinusoids $\hat{I}_m$ is given by \autoref{equation:frida_ihat}, and the generalized FRI sampling framework, \autoref{fig:fri_framework}, shows an \textit{annihilation} step before we can estimate the desired signal parameters.
Hence, $\hat{I}_m$ must satisfy the annihilation equations \begin{equation}
\hat{I}_m * h_m = 0
\end{equation} where $h_m$ is the unknown annihilation filter to be recovered.
In this case, $h_m$ solely depends on the source locations and thus only one $h_m$ has to be found that annihilates $\hat{I}_m(\omega)\, \forall \omega$ \autocite{frida}.

\subsubsection{The Algorithm}
The source locations across exactly $J$ sub-bands with different frequencies $\omega_j$ are reconstructed jointly. Finally, the FRIDA estimate is formulated as a constrained optimization
\begin{gather}\label{equation:frida_estimate}
    \min_{\mathbf{b}_1, \dots, \mathbf{b}_J; \mathbf{h} \in \mathcal{H}} \quad \sum_{i=1}^J \lVert \mathbf{a}_i - \mathbf{G}_i \mathbf{b}_i \rVert_2^2 \\
    \nonumber \text{subject to} \quad \mathbf{b}_i * \mathbf{h} = 0 \quad \text{for}\, i=1, \dots, J.
\end{gather}
where $\mathcal{H}$ is the set that the annihilating filter coefficients belong to \autocite{frida}.
Note that the quadratic minimization problem in \autoref{equation:frida_estimate} can be reformulated by substituting the solution of $\mathbf{b}_i$ in function of $\mathbf{h}$, so we end up with an equation that only contains $\mathbf{h}$:
\begin{equation}\label{equation:frida_estimate_optimized}
\min_{\mathbf{h} \in \mathcal{H}} \quad \mathbf{h}^H \mathbf{\Lambda}(\mathbf{h})\mathbf{h}
\end{equation}
where
\begin{equation}
\mathbf{\Lambda}(\mathbf{h}) = \sum_{i=1}^J \mathbf{T}^H(\boldsymbol{\beta}_i) \begin{bmatrix}
    \mathbf{R}(\mathbf{h})(\mathbf{G}_i^H \mathbf{G}_i)^{-1} \mathbf{R}^H(\mathbf{h})
\end{bmatrix}^{-1} \mathbf{T}(\boldsymbol{\beta}_i)
\end{equation}
and $\boldsymbol{\beta}_i = (\mathbf{G}_i^H \mathbf{G}_i)^{-1} \mathbf{G}_i^H \mathbf{a}_i$; $\mathbf{T}(\cdot)$ builds a Toeplitz matrix from the input vector, $\mathbf{R}(\cdot)$ is the right-dual matrix associated with $\mathbf{T}(\cdot)$ such that $\mathbf{T}(\mathbf{b})\mathbf{h} = \mathbf{R}(\mathbf{h})\mathbf{b}, \, \forall \mathbf{b}, \mathbf{h}$ \autocite{frida}.
Instead of obtaining a convergent solution to \autoref{equation:frida_estimate_optimized}, the solution is calculated up to a certain specified approximation level.
