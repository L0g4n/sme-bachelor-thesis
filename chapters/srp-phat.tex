\section{SRP-PHAT}\label{section:srp_phat}

\textit{SRP-PHAT}, steered response power (SRP) phase transform (PHAT), is here described in the way it has been originally introduced in \autocite{srpphat,dibiase2001robust}.
Basically, \textit{steered response} is the process of using the output of a beamformer to focus the array to particular position in space, in the context of source localization usually the assumed position of the source; \textit{PHAT} refers to the specific weighting function used in the generalized cross correlation (GCC) \autocite{gcc}, more details follow later.

\subsection{Signal Model}
%  Hier Absatz über REVERBERATION
To be able to cope with \textit{reverberation}, that is the production of echoes as the propagating waves reflect off objects in space, which is known to have an enormous effect on the performance of every signal processing system, a \textit{direct-path} only signal model is no longer valid, hence \textit{multi-path} propagation must be incorporated in the signal model. This will be handled by incorporating the \textit{impulse responses} at each specific microphone.
Thus, the received signal at the m-th microphone can be expressed as
\begin{equation}\label{equation:signal_model}
    x_m(t) = s(t) * h_m(\boldsymbol{q}_s, t) + v_m(t)
\end{equation}
with $\boldsymbol{q}_s$ denoting the cartesian coordinates of the source, $s(t)$ being the raw signal of the source, $h_m(q_s, t)$ is the overall impulse response comprised of the room impulse response and the microphone channel response, the additive term $v_m(t)$ represents the noise that is assumed to be uncorrelated with $s(t)$ and $*$ denotes convolution \autocite[chap. 8.3.1]{dibiase2001robust}.
\autoref{equation:signal_model} can then also modified such that the impulse response is rewritten in terms of its direct path component to be able to evaluate the time delay $\tau_m$
\begin{equation}\label{equation:signal_model_tau}
    x_m(t) = \frac{1}{r_m} s(t - \tau_m) * g_m(\boldsymbol{q}_s, t) + v_m(t)
\end{equation}
with $r_m$ being the distance from the microphone to the source, $\tau_m$ is the direct path time delay, and $g_m(\boldsymbol{q}_s, t)$ is the now modified impulse response omitting the direct path component \autocite[chap. 8.3.1]{dibiase2001robust}.

\subsection{Generalized Cross Correlation and PHAT weighting}

For a pair of microphones, $n = 1,2$, the associated \textit{time difference of arrival} (TDOA), $\tau_{12}$, is defined as \autocite[chap. 8.3.2]{dibiase2001robust}:
\begin{equation}\label{equation:tdoa}
    \tau_{12} = \tau_2 - \tau_1
\end{equation}
After applying this definition to their received microphones signals, we get
\begin{gather}\label{equation:mic_signals12}
\nonumber x_1(t) = \frac{1}{r_1} s(t - \tau_1) * g_1(\boldsymbol{q}_s, t) + v_1(t)\\
x_2(t) = \frac{1}{r_2} s(t - \tau_1 - \tau_{12}) * g_2(\boldsymbol{q}_s, t) + v_2(t)
\end{gather}
If the impulse responses for both microphones are similar, according to \autoref{equation:mic_signals12} a scaled version of $s(t - \tau_1)$ exists in the signal from microphone 1 and a time-shifted, scaled version of $s(t - \tau_1)$ is also present in the signal from microphone 2. Now, the \textit{cross correlation} is introduced. It should peak at the very time lag where the shifted versions of $s(t)$ align, corresponding to the resulting TDOA, $\tau_{12}$. It is given by \autocite[chap. 8.3.2]{dibiase2001robust}
\begin{equation}\label{equation:cross_correlation}
    c_{12}(\tau) = \int_{-\infty}^{+\infty} x_1(t)x_2(t + \tau) dt
\end{equation}

The Generalized Cross Correlation (GCC), $R_{12}(\tau)$, is a natural progression of the cross correlation as it contains two filtered versions of $x_1(t)$ and $x_2(t)$. $G_1(\omega)$ and $G_2(\omega)$ denote the Discrete Fourier Transform (DFT) of the filtered versions, respectively, and the GCC function is defined in terms of the Fourier transforms of the microphone signals
\begin{equation}\label{equation:gcc}
    R_{12}(\tau) = \frac{1}{2\pi} \int_{-\infty}^{+\infty} (G_1(\omega) X_1(\omega))(G_2(\omega) X_2(\omega))^* e^{j\omega\tau}d\omega
\end{equation}
with $^*$ denoting the complex conjugate \autocite{gcc}. Now, a frequency dependent weighting function, $\Psi_{12} \equiv G_1(\omega)G_2(\omega)^*$, is introduced and the GCC function is rearranged \autocite[chap. 8.3.2]{dibiase2001robust} with
\begin{equation}\label{equation:gcc_weighting_function}
    R_{12}(\tau) = \frac{1}{2\pi} \int_{-\infty}^{+\infty} \Psi_{12}(\omega) X_1(\omega)X_2(\omega)^*e^{j\omega\tau}d\omega
\end{equation}
$R_{12}(\tau)$ ideally will exhibit a global maximum at the lag value which corresponds to the TDOA. The TDOA estimate is calculated from this specific value with \autocite{brandstein1997robust}
\begin{equation}\label{equation:tdoa_estimate}%
    \hat{\tau}_{12} = \underset{\tau}{\operatorname{argmax}}\,R_{12}(\tau)%
\end{equation}%
The main point of the weighting function $\Psi_{12}$ is to emphasize the very GCC value at the true TDOA value over the other non-desired local maxima since often there is not only one global maximum (e.g., due to reverberation and noise).
Referring back to the start of the chapter, \textit{PHAT} is the specific weighting function defined by
\begin{equation}\label{equation:phat}%
    \Psi_{12}(\omega) \equiv \frac{1}{\lvert X_1(\omega)X_2^*(\omega)\rvert}%
\end{equation}%
which has the characteristic of being less susceptible to reverberation and noise by using only the phase information at each frequency \autocite{brandstein1997robust} and whitening the microphone signals to equally emphasize all frequencies.
Since we are exclusively interested in ultrasonic signals, it seems more suitable to replace the PHAT weighting function by a simple \textit{bandpass weighting function}
\begin{equation}\label{equation:bandpass_weighting}
    \Psi_{12}(\omega) \equiv
    \begin{cases}
    1\quad 2\pi \cdot2000 \le \omega \le 2\pi\cdot 2500 \\
    0\quad otherwise
    \end{cases}
\end{equation}
which attenuates frequencies outside the band of interest \autocite[chap. 4.1.3]{srpphat}. In this example the band of interest is $[2, 2.5]$ kHz.

\subsection{Steered Response Power Localization}

The process of focusing a microphone array on signals originating from a particular location is generally referred to as \textit{Beamforming}. A general \textit{filter-and-sum} beamformer \enquote{[\ldots] applies some temporal filters to the microphone signals before summing them to produce a single, focused signal} \autocite[chap. 6]{srpphat}. A simple \textit{delay-and-sum} beamformer delays the microphone signals $x_m(t)$ so that all versions of the source signal are time-aligned before they are summed in order to preserve the unmodified signal from a given location while attenuating all others \autocite[chap. 8.3.4]{dibiase2001robust}:
\begin{equation}\label{equation:delay_and_sum_beamformer}
    y(t, \boldsymbol{q}_s) = \sum_{m=1}^{M} x_m(t + \Delta_m)
\end{equation}
where $\Delta_m$ are the \textit{steering delays} that focus the array to the location of interest, $\boldsymbol{q}_s$.
An N-element, filter-and-sum beamformer is defined in the frequency domain associated as
\begin{equation}\label{equation:filter_and_sum_beamformer}
    Y(\omega, \boldsymbol{q}) = \sum_{m=1}^{m} G_m(\omega)X_m(\omega)e^{j\omega\Delta_m}
\end{equation}
where $X_m(\omega)$ and $G_m(\omega)$ are the DFTs of the m-th microphone signal and its temporal filter, respectively.
As we are using the output, the steered response, for localizing a source by steering the array to specific locations of interest we are evaluating the output signal by its power \autocite[chap. 8.3.4]{dibiase2001robust}.
Thus, when the focus reaches the true location of the source it should yield a global maximum (again, other maxima also occur due strong reflections and other hostile conditions). Therefore, the search for a potential source location is expressed as the output power of filter-and-sum beamformer by
\begin{equation}\label{equation:srp}
    P(\boldsymbol{q}) = \int_{-\infty}^{+\infty}\lvert Y(\omega) \rvert^2 d\omega
\end{equation}
and the location estimate, $\hat{q}_s$, is found from
\begin{equation}
    \hat{\boldsymbol{q}}_s = \underset{\boldsymbol{q}}{\operatorname{argmax}}\,P(\boldsymbol{q})
\end{equation}

\subsection{The SRP-PHAT Algorithm}
Now, the actual process of the SRP-PHAT algorithm is described.
The main idea behind SRP-PHAT is to \enquote{[\ldots] combine the advantages of the steered beamformer for source localization with the signal and condition independent robustness offered by the PHAT weighting} \autocite[chap. 8.3.5]{dibiase2001robust}.

Now, bringing SRP filter-and-sum beamformer, as seen in \autoref{equation:filter_and_sum_beamformer}, \autoref{equation:srp}, to the multi-channel case, i.e. multiple microphones:
\begin{equation}\label{equation:srp_beamformer}
    P(\boldsymbol{q}) = \sum_{l=1}^{M}\sum_{k=1}^{M} \int_{-\infty}^{+\infty} \Psi_{lk}(\omega)X_l(\omega)X_k^*(\omega)e^{j\omega(\Delta_k - \Delta_l)}d\omega
\end{equation}
where $\Psi_{lk}(\omega)$ is the weighting function used for the two channels \autocite[chap. 8.3.5]{dibiase2001robust}, the PHAT weighting is, as already seen in \autoref{equation:phat}, as follows:
\begin{equation}
    \Psi_{lk} = \frac{1}{\lvert X_l(\omega)X_k^*(\omega)\rvert}
\end{equation}
Alternatively to the frequency-domain expressions in \autoref{equation:srp_beamformer}, the time-domain version of SRP-PHAT is expressed as
\begin{equation}\label{equation:srp_phat_time_domain}
    P(\boldsymbol{q}) = 2\pi\sum_{l=1}^{M}\sum_{k=1}^{M}R_{lk}(\Delta_k - \Delta_l)
\end{equation}
where $R_{lk}(\tau)$ is the PHAT weighting of the two channels. This equation denotes the sum of all possible pairwise GCC permutations time-shifted by exactly the differences in the steering delays $\Delta_m$ \autocite[chap. 8.3.5]{dibiase2001robust}.
Both methods are evaluated by maximizing $P(\boldsymbol{q})$ over a space of potential candidate source locations, usually in the form of a grid with a desired resolution, e.g., one degree of angular resolution per grid point.
A summary of the algorithm for the far-field case is illustrated in \autoref{pseudo:srp_phat}, omitting the distance. $L$ denotes the location space, the grid, resulting from the product of the angular resolution in azimuth and elevation. For example, if a resolution of one degree in azimuth and elevation is desired, the magnitude of L is $\lvert L \rvert = N_\theta N_\phi = 360\cdot180 = 64\,800$ \autocite{generalizedSRP}.
\pseudo{SRP-PHAT}{pseudo:srp_phat}{srp-phat}
