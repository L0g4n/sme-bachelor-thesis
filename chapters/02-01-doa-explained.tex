\chapter{What is the Direction of Arrival?}\label{chapter:doa_explained}

This chapter aims to explain what one can understand when we are talking about the DOA in the context of acoustic source localization and to illustrate some of the basic concepts surrounding it.

Throughout this thesis, vectors and matrices are denoted by bold lower and upper case letters, e.g., $\boldsymbol{x}$ and $\boldsymbol{A}$.
The norm of a vector is denoted by $\lVert \boldsymbol{x} \rVert$. Superscript $T$ and $H$ denote the transpose and Hermitian transpose..
$\mathbb{S}$ denotes the unit circle.

The \textit{direction of arrival} is directly related to the concept of the \textit{direction of propagation}; The latter denotes simply the direction from which acoustic waves from one or more sources impinge our sensors in the microphone array. The DOA is basically the same thing but changing the perspective from observing the source in relation to the array (the direction of propagation) to the opposite by observing the array in relation to the source (the DOA).
The propagation vectors for a single source and a linear microphone array are depicted in \autoref{fig:propagation_vectors}.

\image{.8\textwidth}{propagation_vectors}{
Propagation vectors for an example with a single acoustic source, its direct path signal, and a linear array of four microphones \autocite[chap. 2.6]{srpphat}.
}{fig:propagation_vectors}

$\boldsymbol{d}^{(s)}$ is a vector describing the source location, $\boldsymbol{d}_m$ specifies the location of the m-th microphone, and $\boldsymbol{\zeta}_m^{(s)}$ defines the propagation vector of the corresponding source and microphone \autocite[chap. 2.6.1]{srpphat} with:
\begin{equation*}
\boldsymbol{\zeta}_m^{(s)} = \frac{\boldsymbol{d}_m - \boldsymbol{d}^{(s)}}{\lvert \boldsymbol{d}_m - \boldsymbol{d}^{(s)} \rvert} \quad \forall m \in [1, \ldots, M]
\end{equation*}
Since the DOA is just the opposite of the direction of propagation, it is represented just by $-\boldsymbol{\zeta}_m^{(s)}$.

Another important distinction in acoustic localization regarding the wave field properties is \textit{near field} vs. \textit{far field} \autocite[chap. 2.6.2]{srpphat}. The latter occurs when the dimensions of the array, the \textit{aperture}, are considerably smaller than the distance to the source. Under this condition the waves arriving at the array are said to be planar since the curvature of the propagating waves in relation to the dimensions of the array is small; This also means that the propagation directions are approximately all the same at the array.
On the other hand, a source is said to be located in the near field when the distance from the source is comparable to the aperture of the array. Under this condition the curvature of the propagating waves is significant compared to the dimension of the array. These two conditions result in the important implication that a far-field array \textbf{can not} reconstruct the distance to the source as it contains too little curvature information.
In this thesis we are only considering the far-field case as we are interested in locating remote sources.

Coming back to the DOA, this angle can be hypothetically defined by any point in the array, but is usually chosen to be the center of the microphone array \autocite[chap. 2.6.3]{srpphat}. The center is represented by index $o$; Thus, the propagation vector is $\boldsymbol{\zeta}_o^{(s)}$, the DOA vector $-\boldsymbol{\zeta}_o^{(s)}$. The DOA is often defined in terms of the local coordinate system of the array; A graphical visualization and the resulting angles can be seen in \autoref{fig:doa_angles}.

The two resulting angles, \textit{azimuth} and \textit{elevation}, are defined as follows \autocite[chap. 2.6.3]{srpphat}: Azimuth is the angle between the projection of the DOA vector onto the xy-plane and the local x-axis, elevation is the angle between the DOA vector and the xy-plane.
Hence, the propagation vector (and thereby the DOA vector) can also be defined in terms of these angles:
\begin{equation}
\boldsymbol{\zeta}_m^{(s)} =
    \begin{bmatrix}
        \cos\phi\sin\theta\\
        \cos\phi\cos\theta\\
        \sin\phi
    \end{bmatrix}
\end{equation}

\image{.8\textwidth}{doa_angles}{
The DOA of a single source with its azimuth, $\theta$, elevation, $\phi$, and propagation vector, $\boldsymbol{\zeta}_0^{(s)}$, in a setting with a planar microphone array \autocite[chap. 2.6.3]{srpphat}.
}{fig:doa_angles}
