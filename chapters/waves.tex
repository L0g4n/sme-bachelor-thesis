\section{WAVES}\label{section:waves}

\textit{WAVES}, Weighted Average of Signal Subspaces, is another algorithm for DOA finding of multiple wide-band sources, originally proposed by Claudio and Parisi in \autocite{waves}.
This approach is similar to the one used in \autoref{section:cssm} since it also follows the framework of decomposing the array output in the frequency domain into its narrow-band components and then combining the individual subspaces into a single subspace. First, WAVES obtains the signal subspace eigenvectors from the narrow-band decomposition done before and these vectors and generates from these results a \textit{pseudodata} matrix which is an approximation for the \textit{narrow-band array model}. Subsequently, the WAVES estimate is obtained from the pseudodata matrix through \textit{pseudocovariance}, i.e, instead of simply averaging all narrow-band components like CSSM, WAVES \textit{adaptively weights} the eigenvectors in order to reduce the impact of model errors on the final estimate \autocite{waves}. Also, unlike CSSM, WAVES does not require a preliminary search of the approximate angles by using an angle-dependent error criterion for coherent focusing.

\subsection{Signal Model}

The considered signal model is very similar to the one used in CSSM. The microphone array is assumed to have $M$ elements receiving $D$ signals. Each sensor signal is then converted to the frequency domain and thus decomposed into its narrow-band components consisting of $J$ complex subbands. Each subband snapshot is denoted by $\mathbf{x}_i(nT)$, $T$ denotes the sampling period, and since the bandwidth of each subband is much smaller than its central frequency $f_i (i = 1, 2, \dots, J)$, the classic narrow-band equation
\begin{equation}\label{equation:waves_narrowband}
\mathbf{x}_i(nT) = \mathbf{A}(f_i) \mathbf{s_i}(nT) + \mathbf{n}_i(nT)
\end{equation}
approximately holds where
\begin{equation}\label{equation:waves_transfer_matrix}
\mathbf{A}(f_i) = [ \mathbf{a}_1(f_i, \theta_1), \dots, \mathbf{a}_D(f_i, \theta_D) ]
\end{equation}
is the array transfer matrix at frequency $f_i$ with each column being referred to as \textit{steering vector} representing the array response to each wavefront, $\mathbf{s}_i(nT)$ is the complex source vector, and $\mathbf{n}_i(nT)$ is the noise vector at frequency $f_i$ \autocite{waves}.
The \textit{spatial data covariance matrix} (SCM) at frequency $f_i$ is
\begin{equation}\label{equation:waves_scm}
\mathbf{R}_{xx}(f_i) = \mathbf{A}(f_i) \mathbf{R}_{ss}(f_i) \mathbf{A}^H(f_i) + \sigma_n^2 \mathbf{I}_M
\end{equation}
where $\mathbf{I}_M$ is a $M \times M$ identity matrix. The matrix $\mathbf{E}_s(f_i)$ and $\mathbf{E}_n(f_i)$, composed of the largest/smallest eigenvalues, forms a basis for the signal and noise subspace at frequency $f_i$, respectively.
More precisely, the eigenvectors corresponding to the $\eta_i \leq D$ largest SCM eigenvalues $\lambda_1(f_i) \geq \lambda_2(f_i) \geq \dots \geq \lambda_{\eta_i}(f_i)$ compose $\mathbf{E_s}(f_i)$, the $M - \eta_i$ eigenvectors associated with the smallest eigenvalue $\sigma_n^2$ of $\mathbf{R}_{xx}(f_i)$ $\mathbf{E}_n(f_i)$ \autocite{waves}.
The noise subspace is the orthogonal complement to the signal subspace.

The \textit{pseudodata} $\mathbf{Z}$, built from SCM eigenvectors, is introduced, from its columns the $d$-dimensional subspace is spanned, and hence $\mathbf{Z}$ defines a basis for the universal signal subspace.
This subspace is interpreted as the the average of weighted $\mathbf{E}_s(f_i)$s and is thereby referred to as \textit{WAVES} \autocite{waves}.
\begin{equation}\label{equation:waves_pseudodata_matrix}
\hat{\mathbf{Z}} = \left(\sum_{i=1}^J \eta_i\right)^{-\frac{1}{2}} \cdot \begin{bmatrix}
    \mathbf{T}(f_1)\hat{\mathbf{E}}_s(f_1)\mathbf{P}(f_1), \dots, \mathbf{T}(f_J)\hat{\mathbf{E}}_s(f_J)\mathbf{P}(f_J)
\end{bmatrix} = \eta^{-\frac{1}{2}} \begin{bmatrix}
    \hat{\mathbf{Z}}_1, \dots, \hat{\mathbf{Z}}_J
\end{bmatrix}
\end{equation}

\subsection{The WAVES Algorithm}

A basis for WAVES is estimated from the $d$ principal left singular vectors $\hat{\mathbf{U}}_s$ of the following reduced-size SVD (R-SVD) of $\hat{\mathbf{Z}}$
\begin{equation}\label{equation:waves_rsvd}
\hat{\mathbf{Z}} = \begin{bmatrix}
    \hat{\mathbf{U}}_s & \hat{\mathbf{U}}_N
\end{bmatrix} \begin{bmatrix}
    \hat{\mathbf{\Sigma}_s} & \mathbf{0}\\
    \mathbf{0} & \hat{\mathbf{\Sigma}_N}
\end{bmatrix} \begin{bmatrix}
    \hat{\mathbf{W}}_S & \hat{\mathbf{W}}_N
\end{bmatrix}^H
\end{equation}
which is a total least squares (TLS) fit to restore the original rank-$d$ property of $\mathbf{Z}$ \autocite{waves}. $\hat{\mathbf{U}}_s$ and $\hat{\mathbf{U}}_n$ replace the CSSM subspaces
$\hat{\mathbf{E}}_s$ and $\hat{\mathbf{U}}_N$ in the DOA algorithm \autocite{waves}.
A summary of WAVES can be found in \autoref{pseudo:waves}.

\pseudo{WAVES}{pseudo:waves}{waves}
