\section{CSSM}\label{section:cssm}

\textit{CSSM}, the Coherent Signal-Subspace Method, introduced by Whang and Kaveh in \autocite{cssm}, is another method that relies on subspace decomposition and thus is similar to
MUSIC as described in \autoref{section:music}. However, an important distinction is that CSSM is an extension of the narrow-band case to the wide-band case, i.e., multiple wide-band sources can be localized.
The basic idea of this method to use a coherent signal-subspace estimate obtained by the eigendecomposition of a frequency domain combination of modified narrow-band covariance matrix estimates \autocite{cssm}.
The source signals can be arbitrarily correlated which makes it suitable for heavy multi-path scenarios. By using a coherent signal-subspace estimate, CSSM is classified as a \textit{coherent signal-subspace processing} method.

The steps to perform CSSM are as follows \autocite{cssm}:
\begin{enumerate}
    \item DFT the microphone array signals
    \item Estimate the covariance and transformation matrices using the observed data
    \item Perform a preliminary estimation of the approximate DOAs using a spatial periodogram
    \item Take the estimate of the transformation matrices
    \item Form the estimate of the signal and noise subspace
    \item Form the eigenvalues and corresponding eigenvectors
    \item Estimate the number of sources
    \item Determine peak positions in the spatial spectrum corresponding to the DOAs of the amount of sources
\end{enumerate}
Usually, step 4-8 are iterated a bunch of times to improve the estimates of the resulting DOAs.

\subsection{Signal Model}
Considering an array of $M$ microphones which receive an wavefield generated by $d$ sources in the present of noise, the source signal vector is formulated as
\begin{equation}
\boldsymbol{s}(t) = \begin{bmatrix}
    s_1(t) & s_2(t) & \cdots & s_d(t)
\end{bmatrix}^T
\end{equation}
and is assumed to be stationary over the observation interval $T_0$ with zero mean.
$\boldsymbol{P}_s(f)$ denotes the $d \times d$ unknown source-spectral density matrix. The noise wavefield is assumed to be independent of the source signals and has also a $M \times M$ noise spectral density matrix
$\boldsymbol{P}_n(f)$ but is known in advance. The spectral density matrix of the microphone array, $\boldsymbol{P}_x(f)$, can then be derived from
\begin{equation}
\boldsymbol{P}_x(f) = \boldsymbol{A}(f)\boldsymbol{P}_s(f)\boldsymbol{A}^H(f) + \sigma^2_n \boldsymbol{P}_n(f)
\end{equation}
where $\sigma^2_n$ is the multiplicative constant of the noise spectral density matrix, $\boldsymbol{A}(f)$ is a $M \times d$ transfer matrix from the sources to the microphones with respect to some reference point. Note that it is assumed that the amount of microphones $M$ is bigger than the number of sources $d$.


\subsection{The CSSM Algorithm}
The array output vector $\boldsymbol{x}(t)$ is decomposed into non-overlapping frequency components by the DFT over a time interval of $\Delta T$ and the covariance matrix of the narrow-band component $f_j$ can be expressed as
\begin{gather}
\nonumber cov(\boldsymbol{X}(f_j)) = \frac{1}{\Delta T}\boldsymbol{P}_x(f_j)\\
= \frac{1}{\Delta T} \boldsymbol{A}(f_j) \boldsymbol{P}_s(f_j) \boldsymbol{A}^H(f_j) + \frac{\sigma^2_n}{\Delta T} \boldsymbol{P}_n(f_j), \qquad j=1, \dots, J.
\end{gather}
The array output $\boldsymbol{x}(t)$ observed over $T_0$ seconds is sectioned into $K$ subintervals of duration $\Delta T$ each, thus $\Delta T$ being the duration of a single snapshot and $K$ is the total number of snapshots. $\boldsymbol{X}_k(f_j)$ denotes the $j$-th frequency component obtained from the $k$-th snapshot \autocite{cssm}.

Revising the main idea of the method again, that is, doing a subspace decomposition into narrow-band frequency components, combining the signal subspaces
at different frequencies in order to generate a single subspace that has the algebraic properties of estimating the number of sources and DOAs \autocite{cssm}.

A maximum likelihood (ML) estimation of the covariance matrix can be obtained by taking the snapshot averaged cross-products of $\boldsymbol{X}_k(f_j)$, more specifically
\begin{equation}\label{equation:cssm_chat_ML_estimate}
\hat{\boldsymbol{C}}(\boldsymbol{X}(f_j)) = \frac{1}{K} \sum_{k=1}^{K} \boldsymbol{X}_k(f_j) \boldsymbol{X}_k^H(f_j), \quad j = 1, 2, \dots, J, \quad k = 1, 2, \dots, K.
\end{equation}
The construction of $\boldsymbol{T}(f_j)$, the transformation matrices, requires knowledge of the unknown DOAs which is done using a preliminary, approximate estimation of the angles.
This can be done, for instance, by calculating all periodograms for all narrow bands and then scanning those for spectral peaks \autocite{cssm}. These preliminary angles are denoted by
$\beta_1, \dots, \beta_d$ and $\boldsymbol{A}_\beta(f_j)$ is the $M \times d_1$ preliminary direction matrix of the $j$-th frequency component. \autocite{cssm} develop the estimate of the transformation matrix in the form
\begin{equation}\label{equation:cssm_that_estimate}
\hat{\boldsymbol{T}}(f_j) = [\boldsymbol{A}_\beta(f_0) \rvert \boldsymbol{B}(f_0)]\,[\boldsymbol{A}_\beta(f_j) \rvert \boldsymbol{B}(f_j)]^{-1}, \quad  j = 1, 2, \dots, J.
\end{equation}
A special case occurs when all the true DOAs are within the neighborhood of a single angle $\beta$ (if $d_1 = 1$). In this case, the
estimate of the transformation matrix $\hat{\boldsymbol{T}}(f_j)$ is diagonal in the following form
\begin{equation}\label{equation:cssm_that_simple}
\hat{\boldsymbol{T}}(f_j) = \begin{bmatrix}
    a{_1\beta}(f_0) / a_{1\beta}(f_j) & \cdots & 0\\
    \vdots & a_{2\beta}(f_0) / a_{2\beta}(f_j) & \vdots \\
    0 & \cdots & a_{M\beta}(f_0) / a_{M\beta}(f_j)
\end{bmatrix}
\end{equation}
with $a_{i\beta}(f_j)$ being the $i$-th element of the direction vector $\boldsymbol{A}_\beta(f_j)$.
Now, since $\hat{\boldsymbol{C}}(\boldsymbol{X}(f_j))$ and $\hat{\boldsymbol{T}}(f_j), j = 1, \dots, J$ have been formed, the correlation matrix estimates $\boldsymbol{R}$ and $\boldsymbol{R}_n$ can be formed by
\begin{equation}\label{equation:cssm_rhat_estimate}
\hat{\boldsymbol{R}} = \Delta T \sum_{j=1}^{J} \hat{\boldsymbol{T}}(f_j) \hat{\boldsymbol{C}}(\boldsymbol{X}(f_j)) \hat{\boldsymbol{T}}^H(f_j)
\end{equation}
\begin{equation}\label{equation:cssm_rnhat_estimate}
\hat{\boldsymbol{R}}_n = \sum_{j=1}^{J} \hat{\boldsymbol{T}}(f_j) \boldsymbol{P}_n(f_j) \hat{\boldsymbol{T}}^H(f_j)
\end{equation}
$(\hat{\boldsymbol{R}}, \hat{\boldsymbol{R}}_n)$ are termed as a matrix pencil and used to obtain the estimates of the coherent signal and noise subspace \autocite{cssm}.
A more detailed overview of the algorithm is illustrated in \autoref{pseudo:cssm}. Note that the method for estimating the number of sources, $\hat{d}$, is here not explicitly listed but is done in \autocite{cssm} via the \textit{minimum akaike information criterion estimate} (MAICE).
\pseudo{CSSM}{pseudo:cssm}{cssm}
