$pdf_mode = 1;
@default_files = ('thesis.tex'); # this the main tex file that includes all the other files
$pdflatex = 'pdflatex --shell-escape -interaction=nonstopmode %O %S -file-line-error -synctex=1';

$out_dir = 'build';
$jobname = 'sme-bachelor-thesis';

# enable deletion of *.bbl at "latexmk -c"
$bibtex_use = 2;

#remove more files than in the default configuration
@generated_exts = qw(acn acr alg aux code ist fls glg glo gls idx ind lof loa lot out thm toc tpt);
